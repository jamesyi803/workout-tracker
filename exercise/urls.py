from django.urls import path
from exercise.views import exercise_list, create_exercise

urlpatterns = [
     path("mine", exercise_list, name="exercise_list"),
     path("create/", create_exercise, name="create_exercise")
]
