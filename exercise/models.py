from django.db import models
from django.conf import settings
from week.models import Week

class Exercise(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    sets = models.PositiveIntegerField(null=True)
    reps = models.PositiveIntegerField(null=True)
    weight = models.PositiveIntegerField(null=True)
    minutes = models.PositiveIntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="exercises",
        on_delete=models.CASCADE,
    )
    week = models.ForeignKey(
        Week,
        related_name="exercises",
        on_delete=models.CASCADE,
        null=True
    )
    def __str__(self):
        return self.name
