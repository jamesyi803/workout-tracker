from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from exercise.models import Exercise
from exercise.forms import ExerciseForm

@login_required
def exercise_list(request):
    exercises = Exercise.objects.all()
    context = {"exercises": exercises}
    return render(request, "exercise/list.html", context)


@login_required
def create_exercise(request):
    if request.method == "POST":
        form = ExerciseForm(request.POST)
        if form.is_valid():
            exercise = form.save(False)
            exercise.author = request.user
            exercise.save()
            return redirect("week_detail", id=id)
    else:
        form = ExerciseForm()
    context = {"form": form}
    return render(request, "exercise/create.html", context)
