from django.urls import path
from week.views import week_list, week_detail, create_week


urlpatterns = [
    path("", week_list, name="week_list"),
    path("<int:id>/", week_detail, name="week_detail"),
    path("create/", create_week, name="create_week")
]
