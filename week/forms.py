from django.forms import ModelForm
from week.models import Week


class WeekForm(ModelForm):
    class Meta:
        model = Week
        fields = ["name", "date", "author"]
