from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from week.models import Week
from week.forms import WeekForm


@login_required
def week_list(request):
    weeks = Week.objects.all()
    context = {"weeks": weeks}
    return render(request, "week/list.html", context)


@login_required
def week_detail(request, id):
    week = Week.objects.get(id=id)
    context = {"week": week}
    return render(request, "week/detail.html", context)


@login_required
def create_week(request):
    if request.method == "POST":
        form = WeekForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("week_list")
    else:
        form = WeekForm()
    context = {"form": form}
    return render(request, "week/create.html", context)
