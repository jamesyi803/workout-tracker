from django.db import models
from django.conf import settings


class Week(models.Model):
    name = models.PositiveIntegerField()
    date = models.DateTimeField()
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="weeks",
        on_delete=models.CASCADE,
        null=True
    )
    def __str__(self):
        return str(self.name)
