from django.contrib import admin
from week.models import Week


@admin.register(Week)
class WeekAdmin(admin.ModelAdmin):
    list_display = ("name", "date", "author", "id")
